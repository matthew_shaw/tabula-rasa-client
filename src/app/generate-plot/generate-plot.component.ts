import { Component, OnInit, Input } from '@angular/core';
import { Project } from "../models/project";
import { PlotService } from "../shared/plot.service";
import { IFile } from "../models/file";
import { IPlotObject } from "../models/plot-object";


@Component({
  selector: 'app-generate-plot',
  templateUrl: './generate-plot.component.html',
  styleUrls: ['./generate-plot.component.css'],
  providers: [PlotService]
})
export class GeneratePlotComponent implements OnInit {
  @Input() project: Project;

  options: Object = null;
  selectedDataSource: IFile;
  xAxis: string;
  yAxis: string;
  plotType: string;
  plotName: string;
  
  constructor(private plotService: PlotService) { }

  ngOnInit() { }

  generatePlot() {
    let xAxisIndex = this.selectedDataSource.headers.indexOf(this.xAxis);
    let xAxisData = this.getDataForAxis(xAxisIndex);
    let yAxisIndex = this.selectedDataSource.headers.indexOf(this.yAxis);
    let yAxisData = this.getDataForAxis(yAxisIndex);
    let combinedData = this.combineDataForTwoAxis(xAxisData, yAxisData);

    let plotOptions = {
      chart: {type: 'scatter'},
      title : { text : this.plotName },
      series: [{
        name: this.xAxis + ' vs. ' + this.yAxis,
          data: combinedData,
      }]
    };
    this.options = plotOptions;
  }

  savePlot() {
    let newPlotObject: IPlotObject = {
      name: this.plotName,
      dataSource: this.selectedDataSource.name,
      plotOptions: this.options
    };

    this.plotService.savePlot(this.project._id, newPlotObject).subscribe((saveResponse) => {
      this.project.plotObjects.push(newPlotObject);
      this.plotName = null;
      this.selectedDataSource = null;
      this.options = null;
      this.xAxis = null;
      this.yAxis = null;
      this.plotType = null;    
    });   
  }

  getDataForAxis(axisIndex) {
    let axisData = [];
    for (let row of this.selectedDataSource.fileData) {
      axisData.push(row[axisIndex]);
    }
    return axisData;
  }

  combineDataForTwoAxis(xAxis, yAxis) {
    let combinedData = [];
    for (let idx = 0; idx < xAxis.length; idx++) {
      combinedData.push([parseFloat(xAxis[idx]), parseFloat(yAxis[idx])])
    }

    return combinedData;
  }

}
