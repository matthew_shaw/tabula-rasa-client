import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneratePlotComponent } from './generate-plot.component';

describe('GeneratePlotComponent', () => {
  let component: GeneratePlotComponent;
  let fixture: ComponentFixture<GeneratePlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneratePlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneratePlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
