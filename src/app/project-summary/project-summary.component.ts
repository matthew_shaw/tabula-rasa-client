import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { environment } from "../../environments/environment";
import { Project, IProject } from "../models/project";
import { ProjectService } from "../shared/project.service";
import { IFile } from "../models/file";
import { IPlotObject } from "../models/plot-object";

@Component({
  selector: 'app-project-summary',
  templateUrl: './project-summary.component.html',
  styleUrls: ['./project-summary.component.css'],
  providers: [ProjectService]
})
export class ProjectSummaryComponent implements OnInit {
  @ViewChild('fileInput') fileInput:ElementRef;
  
  constructor(
    private http: HttpClient, 
    private route: ActivatedRoute, 
    private projectService: ProjectService
  ) { }

  project: Project;
  errMsg: string = null;    

  ngOnInit() {
    let projectId = this.route.snapshot.paramMap.get('id');
    this.projectService.getProject(projectId, (project: Project) => {
      let prj = new Project();
      prj.setFields(project)
      this.project = prj;
    });
    
  }

  

  
}
