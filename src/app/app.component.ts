import { Component, ViewEncapsulation } from '@angular/core';
import { MdToolbar } from '@angular/material';
import { Project } from "./models/project";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  constructor() { }
}
