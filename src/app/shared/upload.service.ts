import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";

@Injectable()
export class UploadService {

  constructor(private http: HttpClient) { }

  uploadFile(filePayload: any) {
    // let formData: FormData = new FormData();
    // formData.append("file", file);
    let uploadUrl: string = environment.uploadRoute + "/" + filePayload.projectId;

    return this.http.post(uploadUrl, filePayload);
  }
}
