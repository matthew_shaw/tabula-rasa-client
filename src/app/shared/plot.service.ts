import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { IPlotObject } from "../models/plot-object";

@Injectable()
export class PlotService {
  constructor(private http: HttpClient) { }
  savePlot(projectId: string, plotPayload: IPlotObject) {
    let uploadUrl: string = environment.plotRoute + "/" + projectId;
    return this.http.post(uploadUrl, plotPayload);
  }
}
