import { TestBed, inject } from '@angular/core/testing';

import { FileProcessorService } from './file-processor.service';

describe('FileProcessorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileProcessorService]
    });
  });

  it('should be created', inject([FileProcessorService], (service: FileProcessorService) => {
    expect(service).toBeTruthy();
  }));
});
