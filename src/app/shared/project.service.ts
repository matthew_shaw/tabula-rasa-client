import { Injectable } from '@angular/core';
import { Project } from "../models/project";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ProjectService {

  project: Project;
  
  constructor(private http: HttpClient) { }

  getProject(id: string, callback: any) {
    let projectUrl: string = environment.projectRoute + "/" + id;
    return this.http.get(projectUrl).subscribe((project: Project) => {
      this.project = project;
      callback(this.project);
    });
  }


}
