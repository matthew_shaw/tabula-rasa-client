import { Injectable } from '@angular/core';
import { Project } from "../models/project";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ProjectsService {

  projects: Project[];

  constructor(private http: HttpClient) { }

  createProject(newProject: Project) {
    return this.http.post(environment.projectRoute, newProject);
  }

  getProjectsList(callback: any) {
    this.http.get(environment.projectsListRoute).subscribe((projects: Array<Project>) => {
      this.projects = projects;
      callback(this.projects);
    });
  }

  getProject(id: string) {
    let projectUrl: string = environment.projectRoute + "/" + id;
    return this.http.get(projectUrl);
  }

}
