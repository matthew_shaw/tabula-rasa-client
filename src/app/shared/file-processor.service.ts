import { Injectable } from '@angular/core';

@Injectable()
export class FileProcessorService {

  constructor() { }

  parseFileHeaders(fileContents: string, fileDelimeter: string) {
    let parsedTextLines = fileContents.split("\n");
    return  parsedTextLines.shift().split(fileDelimeter);
  }

  parseFileContents(fileContents: string, fileDelimeter: string) {
    let parsedTextLines = fileContents.split("\n");
    let parsedTextData = [];
    parsedTextLines.shift(); // ignore the first row - it's the header which is being stored separately
    for (let rawLine of parsedTextLines) {
      let line = rawLine.split(fileDelimeter);
      parsedTextData.push(line);
    }
    return parsedTextData;
  }
}
