import { Component, OnInit } from '@angular/core';
import { ProjectsService } from "../shared/projects.service";
import { Project } from "../models/project";

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
  //providers: [ProjectsService]
})
export class ProjectsListComponent implements OnInit {
  projects: Array<Project>;
  constructor(private projectsService: ProjectsService) { }

  ngOnInit() {
    this.projectsService.getProjectsList((projects: Array<Project>) => {
      this.projects = projects;
    });
  }

}
