import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MdToolbarModule, MdCardModule, MdGridListModule, MdInputModule, MdButtonModule, MdSidenavModule, MdListModule, MdIconModule, MdTabsModule, MdMenuModule, MdRadioModule, MdSelectModule } from '@angular/material';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { CreateNewProjectComponent } from './create-new-project/create-new-project.component';
import { ProjectSummaryComponent } from './project-summary/project-summary.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectsService } from "./shared/projects.service";
import { RouterModule, Routes } from '@angular/router';
import { ProjectsHomeComponent } from './projects-home/projects-home.component';
import { HotTableModule } from 'ng2-handsontable';

import { UploadFileComponent } from './upload-file/upload-file.component';
import { FilePreviewComponent } from './upload-file/file-preview/file-preview.component';
import { GeneratePlotComponent } from './generate-plot/generate-plot.component';
import { DataSourceDetailsComponent } from './data-source-details/data-source-details.component';
import { DisplayPlotComponent } from './display-plot/display-plot.component';
import { ProjectDataSourcesComponent } from './project-data-sources/project-data-sources.component';
import { ProjectPlotsComponent } from './project-plots/project-plots.component';

export declare var require: any; // bit of a hack to work around 'require' issue.

const appRoutes: Routes = [
  { path: 'project/:id',      component: ProjectSummaryComponent },
  {
    path: 'projectList',
    component: ProjectsHomeComponent
  },
  { path: '',
    redirectTo: '/projectList',
    pathMatch: 'full'
  },
  { path: '**', component: ProjectsHomeComponent }
];

export function highchartsFactory() {
  const hc = require('highcharts');
  const dd = require('highcharts/modules/drilldown');
  dd(hc);

  return hc;
}

@NgModule({
  declarations: [
    AppComponent,
    CreateNewProjectComponent,
    ProjectSummaryComponent,
    ProjectsListComponent,
    ProjectsHomeComponent,
    UploadFileComponent,
    FilePreviewComponent,
    GeneratePlotComponent,
    DataSourceDetailsComponent,
    DisplayPlotComponent,
    ProjectDataSourcesComponent,
    ProjectPlotsComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    BrowserModule,
    MdToolbarModule,
    MdCardModule,
    MdGridListModule,
    MdInputModule,
    MdButtonModule,
    MdSidenavModule,
    MdListModule,
    MdIconModule,
    MdTabsModule,
    MdMenuModule,
    MdRadioModule,
    MdSelectModule,
    BrowserAnimationsModule,
    HotTableModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    ChartModule
  ],
  providers: [ProjectsService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
