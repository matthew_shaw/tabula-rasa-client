import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectPlotsComponent } from './project-plots.component';

describe('ProjectPlotsComponent', () => {
  let component: ProjectPlotsComponent;
  let fixture: ComponentFixture<ProjectPlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectPlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
