import { Component, OnInit, Input } from '@angular/core';
import { IPlotObject } from "../models/plot-object";
import { IProject } from "../models/project";

@Component({
  selector: 'app-project-plots',
  templateUrl: './project-plots.component.html',
  styleUrls: ['./project-plots.component.css']
})
export class ProjectPlotsComponent implements OnInit {
  @Input() project: IProject;
  selectedPlot: IPlotObject = null;
  
  constructor() { }

  ngOnInit() { }

  plotClicked(plot) {
    this.selectedPlot = plot;
  }

  handlePlotClicked() {
    this.selectedPlot = null;
  }

}
