import { IFile } from "./file";
import { IPlotObject } from "./plot-object";

export interface IProject {
  name: string;
  description: string;
  createdBy: string;
  files: Array<IFile>;
  fileObjects: any;
  plotObjects: Array<IPlotObject>;
  _id: string;
}

export class Project implements IProject {
  name: string = "";
  description: string = "";
  createdBy: string = "";
  files: Array<IFile> = [];
  _id: string = null;
  fileObjects: Array<IFile> = [];
  plotObjects: Array<IPlotObject> = [];

  setFields(project: IProject) {
    this.name = project.name;
    this.description = project.description;
    this._id = project._id;
    this.fileObjects = project.fileObjects;
    

    if (project.createdBy) {
      this.createdBy = project.createdBy;
    } else {
      this.createdBy = "";
    }
    if (project.plotObjects) {
      this.plotObjects = project.plotObjects;
    } else {
      this.plotObjects = [];
    }
    if (project.files) {
      this.files = project.files;
    } else {
      this.files = [];
    }
  }
}