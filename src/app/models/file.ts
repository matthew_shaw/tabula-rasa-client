export interface IFile {
    name: string;
    headers: Array<string>;
    fileData: Array<any>;
    projectId: string;
  }