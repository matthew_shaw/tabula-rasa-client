export interface IPlotObject {
    name: string;
    dataSource: string;
    plotOptions: any;
}