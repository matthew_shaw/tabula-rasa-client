import { Component, OnInit } from '@angular/core';
import { Project } from "../models/project";

@Component({
  selector: 'app-projects-home',
  templateUrl: './projects-home.component.html',
  styleUrls: ['./projects-home.component.css']
})
export class ProjectsHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  showCreateProject: boolean = false;
  showProjectsList: boolean = true;
  project: Project = new Project();
  handleOnCreated(project: Project) {
    this.project = project;
    this.handleViewChange("list_projects");
  }

  handleViewChange(viewToDisplay) {
    if (viewToDisplay === "list_projects") {
      this.showCreateProject = false;
      this.showProjectsList = true;
    } else {
      this.showCreateProject = true;
      this.showProjectsList = false;
    }
  }

}