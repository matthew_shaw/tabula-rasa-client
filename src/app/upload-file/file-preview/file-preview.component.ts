import { Component, OnInit, Input } from '@angular/core';
import { IFile } from "../../models/file";

@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.css']
})
export class FilePreviewComponent implements OnInit {

  @Input() fileInfo;
  
  constructor() { }

  ngOnInit() {
  }

}
