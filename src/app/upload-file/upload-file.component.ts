import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { UploadService } from "../shared/upload.service";
import { IFile } from "../models/file";
import { FileProcessorService } from "../shared/file-processor.service";

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.css'],
  providers: [UploadService, FileProcessorService]
})
export class UploadFileComponent implements OnInit {
  @ViewChild('fileInput') fileInput:ElementRef;
  @Input() projectId: string;
  @Output() fileUploadedEvent: EventEmitter<IFile> = new EventEmitter<IFile>();
  fileDelimeters = [
    {
    val: ',',
    display: 'Comma (.csv)'
    }, {
      val: '\t',
      display: 'Tab (.tsv)'
    }
  ];
  fileDelimeter: string = ",";
  fileData: any = null;
  fileName: string;
  file: IFile = null;
  rawFileText: string = "";

  constructor(private uploadService: UploadService, private fileProcessor: FileProcessorService) { }

  ngOnInit() { }

  handleFileChanged(e: Event) {
    var target: HTMLInputElement = e.target as HTMLInputElement;
    for(var i=0;i < target.files.length; i++) {
        this.upload(target.files[i]);
    }
  }

  private triggerFileInputClick() {
    let event = new MouseEvent('click', {bubbles: true});
    this.fileInput.nativeElement.dispatchEvent(event);
  }

  handleDelimeterChanged(e: Event) {
    this.fileData = this.fileProcessor.parseFileContents(this.rawFileText, this.fileDelimeter);
  }

  upload(file: File) {
    let fr:FileReader = new FileReader();
    var reader: FileReader = new FileReader();
    this.fileName = file.name;
    reader.onload = (e) => {
      this.rawFileText = reader.result;
      this.file = {
        fileData: this.fileProcessor.parseFileContents(this.rawFileText, this.fileDelimeter),
        name: this.fileName,
        projectId: this.projectId,
        headers: this.fileProcessor.parseFileHeaders(this.rawFileText, this.fileDelimeter)
      };
    }
    
    reader.readAsText(file);    
  }

  saveFile() {
    let filePayload: IFile = {
      name: this.file.name,
      fileData: this.file.fileData,
      projectId: this.projectId,
      headers: this.file.headers
    }
    this.uploadService.uploadFile(filePayload).subscribe((fileUploadResponse: any) => {
      this.fileUploadedEvent.emit(filePayload);
    });
  }

}
