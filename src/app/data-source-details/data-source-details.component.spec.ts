import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataSourceDetailsComponent } from './data-source-details.component';

describe('DataSourceDetailsComponent', () => {
  let component: DataSourceDetailsComponent;
  let fixture: ComponentFixture<DataSourceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataSourceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataSourceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
