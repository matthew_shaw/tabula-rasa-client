import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-data-source-details',
  templateUrl: './data-source-details.component.html',
  styleUrls: ['./data-source-details.component.css']
})
export class DataSourceDetailsComponent implements OnInit {
  @Input() fileInfo;
  
  constructor() { }

  ngOnInit() {
  }

}
