import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { Project } from "../models/project";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { ProjectsService } from "../shared/projects.service";

@Component({
  selector: 'app-create-new-project',
  templateUrl: './create-new-project.component.html',
  styleUrls: ['./create-new-project.component.css']
})
export class CreateNewProjectComponent implements OnInit {
  @Output() onCreated = new EventEmitter<Project>();
  
  newProject:Project = new Project();
  isDisabled:boolean = false;
  
  constructor(private projectsService: ProjectsService) { }
  
  ngOnInit() { }

  createProject(){
    this.isDisabled = true;
    this.projectsService.createProject(this.newProject).subscribe((project: Project) => {
      this.projectCreated(project);
    });    
  }
  
  projectCreated(savedProject: Project) {
    this.isDisabled = false;
    this.onCreated.emit(savedProject);
  }
}