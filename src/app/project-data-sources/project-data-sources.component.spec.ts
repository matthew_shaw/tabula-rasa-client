import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDataSourcesComponent } from './project-data-sources.component';

describe('ProjectDataSourcesComponent', () => {
  let component: ProjectDataSourcesComponent;
  let fixture: ComponentFixture<ProjectDataSourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectDataSourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDataSourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
