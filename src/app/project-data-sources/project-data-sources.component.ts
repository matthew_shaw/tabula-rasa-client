import { Component, OnInit, Input } from '@angular/core';
import { IProject } from "../models/project";
import { IFile } from "../models/file";

@Component({
  selector: 'app-project-data-sources',
  templateUrl: './project-data-sources.component.html',
  styleUrls: ['./project-data-sources.component.css']
})
export class ProjectDataSourcesComponent implements OnInit {
  @Input() project: IProject;
  uploadingFile: boolean = false;
  selectedFile: IFile = null;
  
  constructor() { }

  ngOnInit() {
  }

  handleFileUploaded(file) {
    this.project.fileObjects.push(file);
  }

  dataSourceClicked(f: IFile) {
    this.uploadingFile = false;
    this.selectedFile = f;
  }

  handleUploadFileClicked() {
    this.selectedFile = null;
    this.uploadingFile = true;
  }

}
