import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayPlotComponent } from './display-plot.component';

describe('DisplayPlotComponent', () => {
  let component: DisplayPlotComponent;
  let fixture: ComponentFixture<DisplayPlotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayPlotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayPlotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
