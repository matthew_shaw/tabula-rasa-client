import { Component, OnInit, Input } from '@angular/core';
import { IPlotObject } from "../models/plot-object";

@Component({
  selector: 'app-display-plot',
  templateUrl: './display-plot.component.html',
  styleUrls: ['./display-plot.component.css']
})
export class DisplayPlotComponent implements OnInit {
  @Input() plotObject: IPlotObject;
  constructor() { }

  ngOnInit() {
  }

}
