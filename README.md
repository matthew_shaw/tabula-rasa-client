TabulaRasa Client App, built using Angular 4 CLI.

##To run, clone the repository:

git clone https://matthew_shaw@bitbucket.org/matthew_shaw/tabula-rasa-client.git

##Change into the directory:

cd tabula-rasa-client/

##run npm install and bower install:

npm install

bower install

##Then start the built in server:

ng serve

Node JS will need to be installed, along with the Angular CLI and bower package manager.

##The app will make service calls to a Node Express server hosted on heroku: 

https://radiant-fjord-43700.herokuapp.com/api/projects