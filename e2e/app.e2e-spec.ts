import { TabulaRasaClientPage } from './app.po';

describe('tabula-rasa-client App', () => {
  let page: TabulaRasaClientPage;

  beforeEach(() => {
    page = new TabulaRasaClientPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
